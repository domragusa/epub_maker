import re
import time
import uuid
import mimetypes
from html import escape
from pathlib import PurePath, Path
from zipfile import ZipFile, ZIP_DEFLATED, ZIP_STORED

from unidecode import unidecode

from template import get_template


mimetypes.add_type('application/x-dtbncx+xml', '.ncx', strict=False)

def nice_ascii(text, filler='_'):
    text = re.sub('\\W+', filler, unidecode(text.lower()))
    return text.strip(filler)

def unique_entry(already_used, entry, extension=''):
    tmp = entry+extension
    for i in range(1, len(already_used)+1):
        if tmp not in already_used:
            break
        tmp = f'{entry}_{i}{extension}'
    return tmp

class Epub:
    """Class representing the epub archive."""
    def __init__(self, info, uid=None):
        """Constructor.
        
        Parameters:
            info (dict): dictionary containing metadata on the book
                            title:       title of the book,
                            creator:     name of the author,
                            publisher:   publsher,
                            date:        date of publishing,
                            source:      source material (link, isbn),
                            language:    language in ISO 639-1 format,
                            cover:       path to the cover file (jpeg, png)
                            description: a summary or a teaser for the book
            uid (str):   if not set will be generated automatically
        """
        self._numchap = 0
        # root directory, index: content -> fid, index: content -> path
        self._files = ({}, {}, {})
        self._default_root = '/OEBPS'
        
        self._metafiles = tuple(get_template(x) for x in ['opf', 'ncx', 'nav'])
        
        uid = uid or uuid.uuid5(uuid.NAMESPACE_OID, str(info))
        uid = escape(f'urn:uuid:{uid}')
        info = {k: escape(v) for k, v in info.items()}
        
        for f in self._metafiles:
            f.load_data(**info, uid=uid,
                        modified=time.strftime('%Y-%m-%dT%H:%M:%SZ'))
        
        contxml = get_template('containerxml')
        self.add_file('/mimetype', 'application/epub+zip',
                      _compress=False, _index=False)
        self.add_file('/META-INF/container.xml', contxml,
                      _compress=False, _index=False)
        
        opf, ncx, nav = self._metafiles
        ncxid, _ = self.add_file('toc.ncx', ncx)
        opf.load_data(ncxid=ncxid)
        self.add_file('nav.xhtml', nav, _extra=' properties="nav"')
        _, opfpath = self.add_file('content.opf', opf, _index=False)
        
        contxml.load_data(path=opfpath.relative_to('/'))
    
    def add_chapter(self, chapter):
        """Add a chapter (and all its linked_files) to the archive updating the
        spine and the table of content.
        
        Parameters:
            chapter (page): page object."""
        # id must not start with numbers, prefixing with ch_
        fname = f'ch_{nice_ascii(chapter.title)}.xhtml'
        fid, path = self.add_file(fname, chapter)
        parent = path.parent
        
        info = {'id': fid, 'order': self._numchap, 'label': chapter.title,
                'path': str(path.relative_to(parent))}
        for f, param in zip(self._metafiles, ['spine', 'navmap', 'tocli']):
            f.load_data(**{param: info})
        self._numchap += 1
        
        # TODO: generalize this portion to make is usable for every file
        # add referenced files
        for url, ref in chapter.linked_files:
            url = Path(url)
            _, path = self.add_file('extra/'+url.name, url)
            # update the reference to the file
            ref.update(str(PurePath(path).relative_to(parent)))
    
    def _find_parent_directory(self, path: PurePath):
        entry = ('directory', self._files[0])
        for name in path.parent.parts:
            kind, content = entry
            if kind != 'directory':
                raise NotADirectoryError(f'In path {path}, {name} is a file!')
            elif name not in content:
                content[name] = ('directory', {})
            entry = content[name]
        return entry[1]
    
    def _add_to_opf(self, path: PurePath, mime, _extra=''):
        mime, _ = mime or mimetypes.guess_type(path.name, strict=False)
        if mime is None:
            mime, _ = mimetypes.guess_type('file.bin')
        
        name = nice_ascii(str(path.name))
        if name[:1].isdigit():
            name = '_'+name
        
        path = path.relative_to(self._default_root)
        fid = unique_entry(self._files[1].values(), name)
        info = {'id': fid, 'path': str(path), 'mime': mime, 'extra': _extra}
        self._metafiles[0].load_data(manifest=info)
        
        return fid
    
    def add_file(self, path: str, content, *, mime=None, _extra='', _index=True,
                 _compress=True):
        """Add a file to the archive.
        
        Parameters:
            path (str): path *inside* the archive.
            content:    the content of the file, it can be a page-like object,
                        a string or a reference to a real file.
                        a referenceto a file is just a dict specifying a path
                        es. {'path': './dir1/dir2/file.ttf'}
            mime:       file mimetype (specify if not correctly detected)
            
            (INTERNAL)
            _extra:     html code to append as attributes for <item> in the
                        spine, it's used for the toc and the cover
            _index:     whether or not the file should be added to the opf
            _compress:  when True (default) the file is actually compressed
            """
        
        if isinstance(content, Path):
            content = content.resolve()
            kind = 'external'
        else:
            kind = 'regular'
        
        try:
            fid = self._files[1][content]
            path = self._files[2][content]
        except KeyError:
            path = PurePath(path)
            path = path if path.is_absolute() else self._default_root / path
            
            directory = self._find_parent_directory(path)
            name = unique_entry(directory, path.stem, path.suffix)
            attrs = {}
            if not _compress:
                attrs['compress_type'] = ZIP_STORED
            directory[name] = (kind, content, attrs)
            
            fid = self._add_to_opf(path, mime, _extra) if _index else None
            self._files[1][content] = fid
            self._files[2][content] = path
        
        return (fid, path)
    
    def set_cover(self, fid: str):
        """Set a cover.
        
        Parameters:
            fid (str):  file id of the wanted cover.
        
        note: this method does NOT add a cover to the archive, it just sets an
              already added file (image or page) as a cover."""
        if fid not in self._files[1].values():
            raise TypeError(f'file with id {fid} not present in epub')
        self._metafiles[0].load_data(cover=fid)
    
    def _list_files(self, cur_path='/', directory=None):
        cur_path = PurePath(cur_path)
        directory = directory or self._files[0]
        
        for name, (kind, content, *attrs) in directory.items():
            path = cur_path / name
            if kind == 'directory':
                yield from self._list_files(path, content)
            else:
                yield (str(path.relative_to('/')), kind, content, attrs[0])
    
    def write(self, filename: str):
        """Write the epub.
        
        Parameters:
            filename (str): file name (or path) of the output file
        """
        z = ZipFile(filename, 'w', compression=ZIP_DEFLATED)
        for path, kind, content, attrs in self._list_files():
            if kind == 'external':
                z.write(content, arcname=path, **attrs)
            else:
                if not isinstance(content, (str, bytes)):
                    content = content.export()
                z.writestr(path, content, **attrs)
        z.close()
