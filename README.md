# epub maker

Utility and library to create epubs in a relatively (in)sane way.

## Usage

Take a look at the docstrings inside the .py files or just use the help page.

```console
$ txt2epub -h
usage: txt2epub [-h] -i INPUT [-s CSS_FILES [CSS_FILES ...]] [-o OUTPUT]

txt2epub parser

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        Input file.
  -s CSS_FILES [CSS_FILES ...], --css-files CSS_FILES [CSS_FILES ...]
                        CSS files for all chapters.
  -o OUTPUT, --output OUTPUT
                        Output file.
```
