from copy import deepcopy
from string import Formatter

class Multiformatter(Formatter):
    """Formatter with nested definition support.
    
    Here's an exampple:
    ----
    mf = multiformatter()
    somestr = 'something {date:well {{day}}-{{month}}-{{year}}?!} uhmmm'
    #DON'T forget the double parentesis {{day}} is required!
    mf.vformat(somestr, [],{'date':{'day': 1, 'month': 1, 'year': 1970}})
    ----
    
    Or a more interesting one:
    ----
    mf = multiformatter()
    html_list = '<ul{params}>{items:<li>{{value}}</li>}</ul>'
    mf.vformat(html_list, [],{
        'params':' style="color: #FF0;"',
        'items': [
            {'value': 'one'},
            {'value': 'two'},
            {'value': 'three'}
        ]})
    ----
    """
    def format_field(self, value, fmt):
        if fmt=='': return str(value)
        
        # single value put in a list to please map()
        if not isinstance(value, list): value = [value]
        
        # format for all the values
        return ''.join(map(fmt.format_map, value))

class Template:
    """Class that stores the base string and the data to apply."""
    formatter = Multiformatter()
    def __init__(self, base: str, data: dict):
        """Constructor.
        
        Parameters:
        base (str):  string in the format explained in multiformatter
        data (dict): a dictionary with the keys used by the base string
        
        NOTE: data should contain entries for every field present in base
        (empty strings or lists are ok), otherwise they're ignored when trying
        to set them.
        """
        self._base = base
        # deepcopy is important to make sure that different instance of the
        # same template don't interfere with each other
        self._data = deepcopy(data)
    
    def __hash__(self):
        return hash((self.__class__, id(self)))
    
    # NOTE: kwargs is used instead of a simple dictionary to allow calls
    # like x.loaddata(**somevalues, othervalue=1)
    def load_data(self, **data):
        """Load data in the template by a dictionary with keys like the ones
        defined on creation."""
        for k, value in data.items():
            # skip if the key isn't present in the template
            if k not in self._data: continue
            
            # if the field is a list, multiple values are expected and should
            # be all saved (append/concat), if it's not, overwrite
            if isinstance(self._data[k], list):
                if not isinstance(value, list): value = [value]
                self._data[k] += value
            else:
                self._data[k] = value
    
    def reset(self, field: str):
        """Reset a specified field; useful for multivalued ones that can't
        be reset by simply loading a new value (it would be appended)."""
        if k not in self._data: return
        
        if isinstance(self._data[field], list):
            self._data[field] = []
        else:
            self._data[field] = ''
    
    def export(self) -> str:
        """Apply the values and generate the string result."""
        return self.formatter.vformat(self._base, [], self._data)

def get_template(name: str) -> Template:
    """Get a new instance of the desired template."""
    cur = _tpl[name]
    return Template(cur['base'], cur['data'])

_tpl = {
'containerxml': {'base':
'''<?xml version="1.0" encoding="UTF-8"?>
<container version="1.0" xmlns="urn:oasis:names:tc:opendocument:xmlns:container">
    <rootfiles>
        <rootfile full-path="{path}" media-type="application/oebps-package+xml"/>
    </rootfiles>
</container>''',
'data':
    {'path': 'content.opf'}
},

'page': {'base':
'''<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" xml:lang="{language}">
<head>
<meta content="application/xhtml+xml;charset=UTF-8" http-equiv="default-style"/>
<meta charset="UTF-8"/>
<title>{title}</title>
{css:<link type="text/css" rel="stylesheet" href="{{path}}"/>
}</head>
<body>{body}
</body>
</html>''',
'data':
    {'language': 'en', 'title': 'NOTITLE', 'css': [], 'body': ''}
},

'chapter': {'base':
'''<section epub:type="chapter">
{content}
</section>''',
'data':
    {'content': ''}
},

'endnotes': {'base':
'''<section epub:type="endnotes">
    <ol>{notes:
        <li id="{{nid}}" epub:type="endnote">
            <a href="{{ref}}">↑</a> {{text}}
        </li>}
    </ol>
</section>''',
'data':
    {'notes': []}
},

'opf': {'base':
'''<?xml version="1.0" encoding="UTF-8"?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uid" version="3.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/">
        <dc:title id="title">{title}</dc:title>
        <dc:creator>{creator}</dc:creator>
        <dc:publisher>{publisher}</dc:publisher>
        <dc:identifier id="uid">{uid}</dc:identifier>
        <dc:source>{source}</dc:source>
        <dc:date>{date}</dc:date>
        <dc:language>{language}</dc:language>
        <dc:description>{description}</dc:description>
        <meta property="dcterms:modified">{modified}</meta>
        <meta name="cover" content="{cover}"/>{meta:
        <meta name="{{name}}" content="{{content}}"/>}
    </metadata>
    <manifest>{manifest:
        <item id="{{id}}" href="{{path}}" media-type="{{mime}}"{{extra}}/>}
    </manifest>
    <spine toc="{ncxid}">{spine:
        <itemref idref="{{id}}" linear="yes"/>}
    </spine>
</package>''',
'data': 
    {'language': 'en', 'title': 'NOTITLE', 'creator': 'NOBODY', 'publisher': '',  'uid': '', 'source': '', 'cover': '', 'description': '',
     'date': '', 'modified': '', 'ncxid': '', 'meta': [], 'manifest': [], 'spine': []}
},

'nav': {'base':
'''<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" xml:lang="{language}">
<head>
<meta content="application/xhtml+xml;charset=UTF-8" http-equiv="default-style"/>
<meta charset="UTF-8"/>
<title>{title}</title>
</head>
<body>
<section epub:type="frontmatter toc">
<nav epub:type="toc" id="toc">
<ol>{tocli:
<li id="toc-{{id}}"><a href="{{path}}">{{label}}</a></li>}
</ol>
</nav>
</section>
</body>
</html>''',
'data':
    {'title': 'NOTILE', 'language': 'en', 'tocli': []}
},

'ncx': {'base':
'''<?xml version="1.0" encoding="UTF-8"?>
<ncx xmlns="http://www.daisy.org/z3986/2005/ncx/" version="2005-1">
    <head>
        <meta name="dtb:uid" content="{uid}" />
        <meta name="dtb:depth" content="{depth}" />
        <meta name="dtb:totalPageCount" content="{pagecount}" />
        <meta name="dtb:maxPageNumber" content="{maxpage}" />
    </head>
    <docTitle><text>{title}</text></docTitle>
    <docAuthor><text>{creator}</text></docAuthor>
    <navMap>{navmap:
        <navPoint id="{{id}}" playOrder="{{order}}">
            <navLabel><text>{{label}}</text></navLabel>
            <content src="{{path}}"/>
        </navPoint>}
    </navMap>
</ncx>''',
'data':
    {'uid': '', 'depth': '1', 'pagecount': '0', 'maxpage': '0', 'title': 'NOTITLE', 'creator': 'NOBODY', 'navmap': []}
}
}
