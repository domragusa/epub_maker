import re
from html import escape

from template import get_template

class FileReference:
    def __init__(self, ref, key):
        self._ref = ref
        self._key = key
    
    def update(self, val):
        self._ref[self._key] = val

class Page:
    """Class to generate an HTML page with paragraphs, quotes, lists, tables
    and images with support for rich text and foot notes."""
    def __init__(self, *, language='en'):
        self._content = []
        self._notes = []
        self._styles = []
        self.linked_files = []
        self.title = ''
        self.language = language
    
    def __hash__(self):
        return hash((self.__class__, id(self)))
    
    def add_css(self, url: str):
        """Add a CSS to the page."""
        tmp = {'path': url}
        self._styles.append(tmp)
        self.linked_files.append((url, FileReference(tmp, 'path')))
    
    def add_heading(self, text: str, level: int=1):
        """Add a heading."""
        level = min(max(level, 1), 6) # must be between 1 and 6
        attr = {}
        if level == 1: attr['epub:type'] = 'title'
        tmp = text.split('\n', 1)
        if len(tmp)>1:
            tmp = [('#text', {}, self._parse_text(tmp[0])),
                   ('span', {}, self._parse_text(tmp[1]))]
        else:
            tmp = self._parse_text(tmp[0])
        self._content.append((f'h{level}', attr, tmp))
    
    def add_break(self):
        """Add a horizontal breaking."""
        self._content.append(('hr', {}, None))
    
    def add_paragraph(self, p: str, attr={}):
        """Add a text paragraph."""
        self._content.append(('p', attr, self._parse_text(p)))
    
    def add_quote(self, paragraphs: list, attr={}):
        """Add a quote.
        
        Parameters:
            paragraphs (list): list of strings
        """
        tmp = [('p', attr, self._parse_text(text)) for text, attr in paragraphs]
        self._content.append(('blockquote', attr, tmp))
    
    def add_list(self, data: list, attr={}, *, ordered=True):
        """Add a list.
        
        Parameters:
            data (list): elements of the list.
            ordered (bool): type of list ordered or not.
        """
        tmp = [('li', {}, self._parse_text(t)) for t in data]
        tag = 'ol' if ordered else 'ul'
        self._content.append((tag, attr, tmp))
    
    def add_table(self, data: list, attr={}):
        """Add a table."""
        def helper(rowData, cell='td'):
            tmp = [(cell, {}, self._parse_text(t)) for t in rowData]
            return ('tr', {}, tmp)
        buf = []
        if 'head' in data:
            buf.append(('thead', {}, [helper(data['head'], 'th')]))
        if 'body' in data:
            tmp = [helper(row) for row in data['body']]
            buf.append(('tbody', {}, tmp))
        if 'foot' in data:
            buf.append(('tfoot', {}, [helper(data['foot'])]))
        
        self._content.append(('table', attr, buf))
    
    def add_image(self, url, *, pos='center', dim=0.5, alt=None):
        """Add an image."""
        if pos not in ['center', 'right', 'left']: pos = 'center'
        attr = {'src': url, 'class': f'i{pos}', 'width': f'{int(100*dim)}%'}
        if alt is not None: attr['alt'] = alt
        self._content.append(('img', attr, None))
        self.linked_files.append((url, FileReference(attr, 'src')))
    
    def _parse_text(self, text: str, note=True) -> str:
        def helper(m):
            self._notes.append(m.group(1))
            nid = len(self._notes)
            return f'<a epub:type="noteref" id="ref_{nid}" href="#note_{nid}">[{nid}]</a>'
        
        # escape html
        text = escape(text)
        text = re.sub('\n', '<br/>', text)
        
        #substitution of common tags:
        # **bold**, //italic//, __underlined__, ::subscript::, ^^superscript^^
        tags = {'b': '**', 'i': '//', 'u': '__', 'sub': '::', 'sup': '^^'}
        for tag, delim in tags.items():
            pat = '{0}(.*?){0}'.format(re.escape(delim))
            sub = f'<{tag}>\\1</{tag}>'
            text = re.sub(pat, sub, text, flags=re.DOTALL)
        
        # footnotes
        sub = helper if note else ''
        text = re.sub('\{(.*?)\}', sub, text, flags=re.DOTALL)
        
        return text
    
    @classmethod
    def _generate_html(cls, nodes: list):
        buf = ''
        for tag, attr, content in nodes:
            #unpacking attributes
            attrstr = ''.join([f' {name}="{escape(value)}"' for name, value in attr.items()])
            
            if tag == '#text':
                buf += content+'\n'
            elif isinstance(content, str):
                buf += f'<{tag}{attrstr}>\n{content}</{tag}>\n'
            elif isinstance(content, list):
                buf += f'<{tag}{attrstr}>\n{page._generate_html(content)}</{tag}>\n'
            elif content is None:
                buf += f'<{tag}{attrstr}/>\n' #compact tag version
            else:
                raise TypeError(f'{repr(content)} is unsupported')
        
        return buf
    
    def export(self):
        # generating HTML for paragraphs
        tmp = self._generate_html(self._content)
        
        chaptpl = get_template('chapter')
        chaptpl.load_data(content=tmp)
        chap = chaptpl.export()
        
        # generating HTML for footnotes
        notetpl = get_template('endnotes')
        for i, text in enumerate(self._notes):
            notetpl.load_data(notes={'nid': f"note_{i+1}", 'ref': f"#ref_{i+1}", 'text': text})
        notes = notetpl.export()
        
        if self._notes:
            tmp = chap+notes
        else:
            tmp = chap
        
        pagetpl = get_template('page')
        pagetpl.load_data(body=tmp, title=escape(self.title),
                          language=self.language, css=self._styles)
        return pagetpl.export()

if __name__ == '__main__':
    a = Page()
    a.title = 'test'
    a.add_css('main.css')
    a.add_paragraph('Lorem ipsum{ciaoo} dolor sit amet, consectetur adipiscing elit. Curabitur nibh nunc, aliquam non tortor id, elementum euismod libero.')
    a.add_paragraph('This should be treated as text and not html: <b> is the bold tag.')
    a.add_paragraph('Nam vitae enim tempor sem molestie accumsan. Mauris dignissim maximus nisi. Maecenas orci leo, faucibus et accumsan ut, hendrerit non urna. '*5)
    a.add_list(['ciao', 'come', 'va'], ordered=False)
    a.add_table({'head': ['name', 'surname', 'phone'],
            'body': [['domenico{uhmmm}', 'ragusa', '0012810'],
                     ['mario', 'rossi', '92137'],
                     ['giuseppe', 'verdi', '28937']]})
    a.add_heading('Lorem!\nsubtitle', 1)
    a.add_heading('Lorem!\nsubtitle', 2)
    a.add_heading('Lorem!\nsubtitle', 3)
    a.add_paragraph('Ut non rutrum sapien, vel aliquet erat. Sed suscipit turpis eget massa luctus aliquet. Duis molestie tempor odio nec varius.'*3)
    a.add_image('./pic.jpeg', alt='description of pic.jpeg')
    a.add_break()
    a.add_quote([('test1',{}), ('test2',{}), ('test3',{})])
    a.add_paragraph('Nullam feugiat nunc lacus, sit amet accumsan urna pulvinar in. Donec tempor nisl ac turpis egestas, gravida lobortis sem aliquet. ')
    print(a.export())
