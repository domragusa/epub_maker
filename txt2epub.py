#!/usr/bin/env python3

import sys
from pathlib import Path
from argparse import ArgumentParser

from page import Page
from epub import Epub, nice_ascii

def iter_chunk(path):
    off, buf = 0, None
    for n, line in enumerate(open(path, 'r')):
        if line == '\n': line = 1
        if type(line) != type(buf):
            if buf is not None:
                yield (off, buf)
            off, buf = n, line
        else:
            buf += line
    if buf is not None:
        yield (off, buf)

class ParsingError(Exception):
    def __init__(self, message, lineno):
        self.message = message
        self.lineno = lineno

class Parser:
    def __init__(self, *, css_files=None):
        self.epub = None
        self.chapter = None
        self.css_files = css_files or []
        self.meta = {}
    
    def _prepare_cover(self):
        path = Path(self.meta['cover'])
        fid, _ = self.epub.add_file(path.name, path,
                                    _extra=' properties="cover-image"')
        self.epub.set_cover(fid)
        cover_page = Page()
        cover_page.language = self.meta['language']
        cover_page.title = self.meta['title']
        cover_page.add_image(self.meta['cover'], dim=1.)
        self.epub.add_chapter(cover_page)
    
    def _process_metadata(self, source):
        chunk = next(source)[1].rstrip()
        allowed_fields = {'title', 'creator', 'date', 'publisher', 'source',
                          'language', 'cover', 'description'}
        for n, line in enumerate(chunk.split('\n')):
            try:
                name, value = [v.strip() for v in line.split(':', 1)]
                if name not in allowed_fields:
                    raise ParsingError(f'unknown field "{name}"', n)
                if name in self.meta:
                    raise ParsingError(f'duplicate "{name}"', n)
                self.meta[name] = value
            except ValueError:
                raise ParsingError('malformed metadata', n)
        
        mandatory_fields = {'title', 'language'}
        for name in mandatory_fields:
            if name not in self.meta:
                raise ParsingError(f'missing field "{name}"', 0)
        
        self.epub = Epub(self.meta)
    
    @staticmethod
    def _helper_paragraph(text):
        attr = {}
        for align in ['center', 'right', 'left']:
            if text.startswith(f'#{align} '):
                text = text.split(' ', 1)[1]
                attr['class'] = f'p{align}'
        return (text.strip(), attr)
    
    def _process_chunk(self, chunk):
        head, tail = chunk.split(' ', 1)
        if head == '!title':
            self.chapter.title = tail
        elif head[0:2] == '!!':
            self.chapter.add_heading(tail, len(head)-1)
        elif head == '!break':
            self.chapter.add_break()
        elif head == '!image':
            url, alt = tail.split('||', 2)
            self.chapter.add_image(url, pos='center', alt=alt, dim=0.8)
        elif head == '!quote':
            tmp = [self._helper_paragraph(t) for t in tail.split('\n')]
            self.chapter.add_quote(tmp)
        elif head == '!table':
            tmp = [row.split('||') for row in tail.split('\n')]
            self.chapter.add_table({'body': tmp})
        else:
            if head.startswith('!'):
                raise SyntaxError(f'unrecognized {head}')
            text, attr = self._helper_paragraph(chunk)
            self.chapter.add_paragraph(text, attr)
    
    def _insert_chapter(self):
        if self.chapter is not None:
            self.epub.add_chapter(self.chapter)
        
        self.chapter = Page()
        self.chapter.language = self.meta['language']
        
        for css in self.css_files:
            self.chapter.add_css(css)
    
    def _process_chapters(self, source):
        chapter = None
        for line, chunk in source:
            if isinstance(chunk, str): # data chunk
                self._process_chunk(chunk.rstrip())
            else:                      # blanks
                if chunk == 3:   # 3 blanks between chapters
                    self._insert_chapter()
                elif chunk != 1: # 1 blank between paragraphs/commands
                    raise ParsingError(f'unexpected {chunk} blank(s)', line)
        self._insert_chapter()   # last chapter
    
    def process_file(self, in_file):
        source = iter_chunk(in_file)
        try:
            self._process_metadata(source)
            if 'cover' in self.meta:
                self._prepare_cover()
            self._process_chapters(source)
        except ParsingError as e:
            sys.exit(f'ERROR in {in_file}:{e.lineno+1}, {e.message}')
    
    def write_epub(self, out_file=None):
        if out_file is None:
            out_file = nice_ascii(self.meta['title'])
        self.epub.write(out_file)

def default_parser():
    parser = ArgumentParser(description='txt2epub parser')
    parser.add_argument('-i','--input', required=True,
        help='Input file.')
    parser.add_argument('-s', '--css-files', nargs='+', default=[],
        help='CSS files for all chapters.')
    parser.add_argument('-o', '--output', default=None,
        help='Output file.')
    return parser

if __name__ == '__main__':
    parser = default_parser()
    args = parser.parse_args()
    
    txt_parser = Parser(css_files=['main.css']+args.css_files)
    txt_parser.process_file(args.input)
    txt_parser.write_epub(args.output)
    
